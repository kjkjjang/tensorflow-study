"""
ch2
텐서플로우에 대한 이해
 - 1. 그래프 생성 / 2. 그래프 실행
 - 텐서플로우의 모든 객체는 텐서라는 단위로 이루어짐 (그래프를 만드는 하나하나의 과정) -> 텐서의 흐름
"""
import tensorflow as tf

hello = tf.constant("hello, tensorflow")
print(hello)

a = tf.constant(10)
b = tf.constant(20)
c = tf.add(a, b)
print(c)

sess = tf.Session()

print(sess.run(hello))
print(a, b, c)
print(sess.run(a))
print(sess.run(b))
print(sess.run(c))
print(sess.run([a, b, c]))

sess.close()