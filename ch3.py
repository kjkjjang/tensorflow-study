"""
ch3
선형회귀 모델 (Linear Regression)
 - https://ko.wikipedia.org/wiki/선형_회귀
 - y = ax + b 의 형식 으로 a, x, b 를 파악한 후 x 의 값이 주어졌을 때 Y의 값을 구하기
"""

# module import (tensorflow module)
import tensorflow as tf

# 변수 선언
x_data = [1, 2, 3]
y_data = range(1, 4)

# for 문 사용법 (range)
for y in y_data :
    print (y)

# tensorflow 변수 선언 (균등분포 사용 : http://math7.tistory.com/42 참조 )
W = tf.Variable(tf.random_uniform([1], -1.0, 1.0))
b = tf.Variable(tf.random_uniform([1], -1.0, 1.0))

# tensorflow 매개변수 선언 (나중에 값을 채울 예정으로 사용)
X = tf.placeholder(tf.float32, name="X")
Y = tf.placeholder(tf.float32, name="Y")

# 수식 작성
hypothesis = W * X + b

# 손실 함수 작성 (거리 측정법)
cost = tf.reduce_mean(tf.square(hypothesis - Y))
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
train_op = optimizer.minimize(cost)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    # cost 값이 줄어들면 (오차값) 학습이 잘 되고 있다는걸 추측 할 수 있다.
    for stop in range(100):
        _, cost_val = sess.run([train_op, cost], feed_dict={X: x_data, Y: y_data})
        print(stop, cost_val, sess.run(W), sess.run(b))

    # 학습 시킨 결과를 토대로 내가 원하는 임의의 값에 대한 결과를 확인
    print("\n==== TEST ====")
    print("X: 5, Y: ", sess.run(hypothesis, feed_dict={X: 5}))
    print("X: 2.5, Y: ", sess.run(hypothesis, feed_dict={X: 2.5}))