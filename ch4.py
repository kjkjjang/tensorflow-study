import tensorflow as tf
import numpy as np

# [털 유무, 날개 유무]
x_data = np.array([ [0,0], [1,0], [1,1], [0,0], [0,0], [0,1] ])

"""
# 원-핫 인코딩 
  [1, 0, 0] => 해당없음 (기타)
  [0, 1, 0] => 포유류
  [0, 0, 1] => 조류
# x_data 에 따른 구분을 데이터로 미리 학습시킬 용도 
"""
y_data = np.array(
    [
    [1, 0, 0],
    [0, 1, 0],
    [0, 0, 1],
    [1, 0, 0],
    [1, 0, 0],
    [0, 0, 1]
    ]
)

X = tf.placeholder(tf.float32)
Y = tf.placeholder(tf.float32)

# 신경망 1단계 2*10 크기로 결과는 10개가 나온다
W1 = tf.Variable(tf.random_uniform([2, 10], -1., 1.))
# 신경망 2단계 10*3 크기로 결과는 3개가 나온다 (원-핫 인코딩으로 추출하기 위함)
W2 = tf.Variable(tf.random_uniform([10, 3], -1., 1.))

b1 = tf.Variable(tf.zeros([10]))
b2 = tf.Variable(tf.zeros(3))

# Y = WX + B 형식으로 layer 1단계 공식 적용
Layer1 = tf.add(tf.matmul(X, W1), b1)
# 활성화 함수 적용 (여러가지 선택 사항 중 1개만 고르고 나머지는 포기하기 위함 -> 카테고리 분류에 적합)
# 활성화 함수 참고 : http://blog.naver.com/PostView.nhn?blogId=wideeyed&logNo=221017173808&parentCategoryNo=&categoryNo=49&viewDate=&isShowPopularPosts=false&from=postView
Layer1 = tf.nn.relu(Layer1)


Layer2 = tf.add(tf.matmul(Layer1, W2), b2)

# Y(예측값) 과 Layer2(실제 데이터)의 차이 최소화
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=Layer2))
# optimizer 종류
optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
train_op = optimizer.minimize(cost)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    for step in range(100):
        sess.run(train_op, feed_dict={X:x_data, Y: y_data})

        if step % 10 == 0:
            print(step, sess.run(cost, feed_dict={X:x_data, Y:y_data}))

    prediction = tf.argmax(Layer2, 1)
    target = tf.argmax(Y, 1)

    print("예측[", sess.run(prediction, feed_dict={X:x_data, Y:y_data}))
    print("실제[", sess.run(target, feed_dict={Y:y_data}))

    # 조건 데이터 추가
    predict_data = np.array([ [0,0] ])
    print("내가만든데이터[", sess.run(prediction, feed_dict={X: predict_data, Y: y_data}))

    is_correct = tf.equal(prediction, target)
    accuracy = tf.reduce_mean(tf.cast(is_correct, tf.float32))
    print("정확도 %.2f" % sess.run(accuracy * 100, feed_dict={X:x_data, Y:y_data}))